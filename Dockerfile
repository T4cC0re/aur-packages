# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM ubuntu:20.04

RUN mkdir -p /opt/archbuild && apt update && apt install -y curl git && curl -sSL https://get.docker.com | bash && git clone "https://gitlab.com/T4cC0re/archbuild.git" archbuild && cp archbuild/build.sh /opt/archbuild

WORKDIR /opt/archbuild
